
package es.anamarquez.myproject.ui;


import java.util.Scanner;

import es.anamarquez.myproject.entities.articulos;
import es.anamarquez.myproject.entities.discos;
import es.anamarquez.myproject.entities.libros;

import java.util.ArrayList;
public class principal {
	
	static ArrayList<articulos> lista = new ArrayList<articulos>();
	static Scanner teclado = new Scanner(System.in);
	
    public static void main(String[] args) {
        int opcion_menu=100,paginas = 0,duracion = 0,numero_canciones = 0,opcion_modificar=100;
        String titulo = "",autor = "",editorial= "",cantante= "";
        double precio=0;
     
        articulos l = new libros("Salamandra",300,"J.K.Rowling","Harry Potter");
        lista.add(l);
        articulos e = new libros ("SM",268,"Pedro Gonzalez","El nombre de la rosa");
        lista.add(e);
        articulos d = new discos(120,10,10.8,"Dani Martin","Mentiras");
        lista.add(d);
        articulos x = new discos(320,15,20,"Joaquin Sabina","500 noches");
        lista.add(x);
        
        while(opcion_menu!=0)
	        {
	        	
		        System.out.println("�Qu� acci�n quiere realizar?");
		        System.out.println("0.salir");
		        System.out.println("1.Añadir libro");
		        System.out.println("2.Añadir disco");
		        System.out.println("3.¿De qué libro/disco quieres ver la info?");
		        System.out.println("4.Mostrar todos los libros y discos");
		        System.out.println("5.Modificar información de un libro o disco");
		        System.out.println("6.Eliminar un libro o disco");
		       
		        opcion_menu = teclado.nextInt();
		        
		        System.out.printf("Ha elegido usted la opci�n %d %n", opcion_menu);
		        
		        switch (opcion_menu) 
			        {
			        	case 1:
                                newBook(titulo, autor, editorial,paginas);
                                break;
                                        
                        case 2:
                        		newDisc(cantante, titulo, duracion, numero_canciones, precio);	
                        		break;
                        		
                        case 3:
                                teclado.nextLine();
                                System.out.println("Introduzca el nombre del cantante o autor");
                                String cantante_autor = teclado.nextLine();
                                System.out.println("Introduzca el t�tulo del libro o disco");
                                titulo = teclado.nextLine();
                                getLibro_Disco(cantante_autor,titulo,lista);
                                break;
                                
                        case 4:
                                mostrarTodo(lista);
                                break;
                                         
                        case 5:                                                                          
                        		modInfor(opcion_modificar); 
                                break;
                                            
                        case 6:
                                deleteElement();
                                break;
		
			        }
	        }
    }
   public static void mostrarTodo(ArrayList Lista) {
	   for(int i = 0 ; i<Lista.size(); i++)
            {
                System.out.println(Lista.get(i).toString());
             }
   }
   
   public static int getPosicionElemento(String cantante_autor,String titulo, ArrayList Lista) {
		int posicion=-1;
		for(int i = 0 ; i<Lista.size(); i++) {
			articulos aux= (articulos) Lista.get(i);
			if(cantante_autor.equals(aux.getCantante_autor()) && titulo.equals(aux.getTitulo())) {
				posicion = i;				
				break;			
			}
		}
		return posicion;
   }
    
	public static void getLibro_Disco(String cantante_autor,String titulo, ArrayList Lista) {
		int existe = 0;
		for(int i = 0 ; i<Lista.size(); i++) {
			articulos aux= (articulos) Lista.get(i);
			if(cantante_autor.equals(aux.getCantante_autor()) && titulo.equals(aux.getTitulo())) {
				System.out.println("Lo he encontrado");
				existe = 1;
				try {
					System.out.println("Lo voy a formatear");
					discos d = (discos)aux;
					System.out.println("Esto es un disco");
					System.out.println(d.toString());
					}
				catch (Exception e) {					
					System.out.println("Esto es un libro");
					libros l = (libros)aux;
					System.out.println(l.toString());
				}
			
			}
                      
		}
		if (existe == 0) {
			System.out.println("El libro/disco no existe");
		}
		
	}
	
	public static void newBook(String titulo, String autor, String editorial, int paginas)
		{
			teclado.nextLine();
	        System.out.println("Escriba el titulo del libro");
	        titulo = teclado.nextLine();
	        System.out.println("Escriba el autor/a");
	        autor = teclado.nextLine();
	        System.out.println("Escriba la editorial");
	        editorial = teclado.nextLine();
	        System.out.println("Escriba número de páginas");
	        paginas = teclado.nextInt();
	        articulos laux = new libros(editorial, paginas, autor, titulo);
	        lista.add(laux);
		}
	
	public static void newDisc(String cantante, String titulo, int duracion, int numero_canciones, double precio)
		{
			teclado.nextLine();
	        System.out.println("Escriba el cantante");
	        cantante = teclado.nextLine();
	        System.out.println("Escriba el titulo del disco");
	        titulo = teclado.nextLine();
	        System.out.println("Escriba la duracion en minutos");
	        duracion = teclado.nextInt();
	        System.out.println("Escriba número de canciones");
	        numero_canciones = teclado.nextInt();
	        System.out.println("Escriba el precio");
	        precio = teclado.nextDouble();                                            
	        articulos daux = new discos(duracion, numero_canciones, precio, cantante, titulo);
	        lista.add(daux);
		}
	
	public static void modInfor(int opcion_modificar)
		{
			while(opcion_modificar!=0)
		        {
		            teclado.nextLine();
		            System.out.println("Introduzca titulo que desea modificar");
		            String titulo_aux =teclado.nextLine();
		            System.out.println("Introduzca el autor");
		            String autor_aux =teclado.nextLine();
		            int posicion =getPosicionElemento(autor_aux, titulo_aux, lista);
		            if(posicion>=0) {
		            	if (lista.get(posicion)instanceof discos)
		                    {opcion_modificar=2;}
		                else
		                    {opcion_modificar=1;}
		                                                              
		            int opcion_modificacion_libro=100;
		            int opcion_modificacion_disco=100;
		            
		            switch(opcion_modificar)
		                {
		                    case 1:
		                    	System.out.println("�Qu� quiere modificar?");
		                    	System.out.println("1- Autor");
		                    	System.out.println("2- Titulo");
		                    	System.out.println("3- N�mero p�ginas");
		                    	System.out.println("4- Editorial");
		                        System.out.println("0- Salir");
		                        opcion_modificacion_libro = teclado.nextInt();
		                    	
		                        switch(opcion_modificacion_libro)
		                            {
		                            	case 1:
		                            		teclado.nextLine();
		                            		System.out.println("Introduzca autor");
		                            		String autor_modificado = teclado.nextLine();
		                                    ((libros) lista.get(posicion)).setCantante_autor(autor_modificado);
		                            			                                                                		
		                            		break;
		                            	case 2:
		                            		teclado.nextLine();
		                            		System.out.println("Introduzca titulo");
		                            		String titulo_modificado = teclado.nextLine();
		                                    ((libros) lista.get(posicion)).setTitulo(titulo_modificado);	                                                                                                             		
		                            		break;
		                            	case 3:
		                            		teclado.nextLine();
		                            		System.out.println("Introduzca n�mero p�ginas");
		                            		int paginas_modificado = teclado.nextInt();
		                                    ((libros) lista.get(posicion)).setPaginas(paginas_modificado);                              		
		                            		break;
		                            	case 4:
		                            		teclado.nextLine();
		                            		System.out.println("Introduzca editorial");
		                            		String editorial_modificado = teclado.nextLine();
		                                    ((libros) lista.get(posicion)).setEditorial(editorial_modificado);                                          		
		                            		break;
		                            }
		                        break;
		
		                    case 2:
		                    	System.out.println("�Qu� quiere modificar?");
		                    	System.out.println("1- Cantante");
		                    	System.out.println("2- Titulo");
		                    	System.out.println("3- N�mero canciones");
		                    	System.out.println("4- Duraci�n");
		                    	System.out.println("5- Precio");
		                        System.out.println("0- Salir");
		                        opcion_modificacion_disco = teclado.nextInt();
		                        
		                        switch(opcion_modificacion_disco)
		                            {
		                            	case 1:
		                            		teclado.nextLine();
		                            		System.out.println("Introduzca cantante");
		                            		String cantante_modificado = teclado.nextLine();
		                            		((discos) lista.get(posicion)).setCantante_autor(cantante_modificado);                                                          		
		                            		break;
		                            	case 2:
		                            		teclado.nextLine();
		                            		System.out.println("Introduzca titulo");
		                            		String titulo_modificado = teclado.nextLine();
		                                    ((discos) lista.get(posicion)).setTitulo(titulo_modificado);                                            		
		                            		break;
		                            	case 3:
		                            		System.out.println("Introduzca n�mero canciones");
		                            		int canciones_modificado = teclado.nextInt();
		                                    ((discos) lista.get(posicion)).setNumero_canciones(canciones_modificado);                                          		
		                            		break;
		                            	case 4:
		                            		System.out.println("Introduzca duracion");
		                            		int duracion_modificado = teclado.nextInt();
		                                    ((discos) lista.get(posicion)).setDuracion(duracion_modificado);                              		
		                            		break;
		                            	case 5:
		                            		System.out.println("Introduzca precio");
		                            		double precio_modificado = teclado.nextDouble();
		                                    ((discos) lista.get(posicion)).setPrecio(precio_modificado);                                         		
		                            		break;
		                            }
		                        break;
		                }
		            break;
		        }
		            else {
		            	System.out.println("El elemento que busca no existe en la lista");
		            	break;
		            }
		        }
		}
	
	public static void deleteElement()
		{
			 teclado.nextLine();
	         System.out.println("Introduzca titulo que desea modificar");
	         String titulo_aux =teclado.nextLine();
	         System.out.println("Introduzca el autor");
	         String autor_aux =teclado.nextLine();
	         int posicion =getPosicionElemento(autor_aux, titulo_aux, lista);
	         if(posicion>=0)
	             {
	         		lista.remove(posicion);
	             }
	         else
	             {
	             	System.out.println("El libro que desea eliminar no est� en la lista");
	             }
		}
}