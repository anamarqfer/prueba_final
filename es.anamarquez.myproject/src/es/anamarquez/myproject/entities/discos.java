
package es.anamarquez.myproject.entities;


public class discos extends articulos{
    
    private int duracion;
    private int numero_canciones;
    private double precio;

    public discos(int duracion, int numero_canciones, double precio, String cantante_autor, String titulo) {
        super(cantante_autor, titulo);
        this.setDuracion(duracion);
        this.setNumero_canciones(numero_canciones);
        this.setPrecio(precio);
    }

    
  
/***** GETTERS Y SETTERS *****/    
   
    public int getDuracion() {
        return duracion;
    }

    public void setDuracion(int duracion) {
        this.duracion = duracion;
    }

    public int getNumero_canciones() {
        return numero_canciones;
    }

    public void setNumero_canciones(int numero_canciones) {
        this.numero_canciones = numero_canciones;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    @Override
    public String toString() {
        return super.toString() + "discos{" + "duracion=" + duracion + ", numero_canciones=" + numero_canciones + ", precio=" + precio + '}';
    }

}
