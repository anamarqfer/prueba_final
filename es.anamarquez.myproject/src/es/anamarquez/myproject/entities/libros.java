
package es.anamarquez.myproject.entities;

public class libros extends articulos{

    private String editorial;
    private int paginas;

    public libros(String editorial, int paginas, String cantante_autor, String titulo) {
        super(cantante_autor, titulo);
        this.setEditorial(editorial);
        this.setPaginas(paginas);
    }


/***** GETTERS Y SETTERS *****/

    public String getEditorial() {
        return editorial;
    }

    public void setEditorial(String editorial) {
        this.editorial = editorial;
    }

    public int getPaginas() {
        return paginas;
    }

    public void setPaginas(int paginas) {
        this.paginas = paginas;
    }

    @Override
    public String toString() {
        return super.toString() + "libros{" + "editorial=" + editorial + ", paginas=" + paginas + '}';
    }

}
