
package es.anamarquez.myproject.entities;


public abstract class articulos {
    private String cantante_autor;
    private String titulo;

    public articulos(String cantante_autor, String titulo) {
        this.setCantante_autor(cantante_autor);
        this.setTitulo(titulo);
    }
    
   

/***** GETTERS Y SETTERS *****/
    public String getCantante_autor() {
        return cantante_autor;
    }

    public void setCantante_autor(String cantante_autor) {
        this.cantante_autor = cantante_autor;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }



	@Override
	public String toString() {
		return "padre [cantante_autor=" + cantante_autor + ", titulo=" + titulo + "]";
	}
   
    
}
